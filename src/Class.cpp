#include "Class.h"

Client::Client(string N, string P)
{
    Nom = N;
    Prenom = P;
}

void Client :: AfficherClient()
{
    cout<<"Nom : "<<Nom<<endl<<"Prenom : "<<Prenom<<endl;
}

Compte::Compte()
{
    Solde = 0;
}

void Compte :: CrediterSolde(float thune)
{
    Solde = Solde + thune;
}

void Compte :: DebiterSolde(float thune)
{
    Solde = Solde - thune;
}

void Compte :: AfficherSolde()
{
    cout<<endl<<"Le solde de votre compte est de : "<<Solde<<" €"<<endl;
}

CompteEpargne :: CompteEpargne()
{
    cout<<endl<<endl<<"###Bienvenue dans la creation d\'un compte epargne ###"<<endl;
    cout<<"Type de compte : ";
    cin>>Type;
    cout<<endl<<"Plafond : ";
    cin>>Plafond;
    cout<<endl<<"Taux : ";
    cin>>Taux;
    cout<<endl<<"Le compte est-il bloque ?"<<endl<<"1- Oui, 2- Non"<<endl;
    int Rep;
    cin>>Rep;
    if(Rep == 1)
    {
        Bloquer == true;
    }
    else
    {
        Bloquer == false;
    }
    float montant;
    cout<<endl<<"Solde : ";
    cin>>montant;
    CrediterSolde(montant);
}

void CompteEpargne :: AfficherCompte()
{
    cout<<"### Infos Compte ###"<<endl;
    cout<<"Type : "<<Type<<endl<<"Taux : "<<Taux<<endl<<"Plafond : "<<Plafond<<endl<<"Compte bloquer ? "<<Bloquer<<endl;
}


void CompteEpargne :: AfficherTitre()
{
    cout<<Type<<endl;
}

void Client :: AfficherComptesEpargne()
{
    cout<<endl<<endl<<endl<<"Comptes :"<<endl;
    list<CompteEpargne>::iterator it;
      for(it=Epargne.begin();it!=Epargne.end();it++)
   {
       (*it).AfficherTitre();
   }
   cout<<endl<<endl;
}

void Client :: AfficherSoldeCE(string t)
{
    list<CompteEpargne>::iterator it;
    for(it=Epargne.begin();it!=Epargne.end();it++)
    {
        if(((*it).ReturnType()) == t)
        {
            (*it).AfficherSolde();
        }
    }
}
