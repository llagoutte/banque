#include "Class.h"

using namespace std;

int main()
{
    Client Un("LAGOUTTE", "Lorenzo");
    int Choix;
    float Credit;
    while (1)
    {
        cout<<"### Menu : ###"<<endl<<"1 - Afficher les informations"<<endl<<"2 - Afficher le solde du compte courant"<<endl
        <<"3 - Crediter le solde du compte courant"<<endl<<"4 - Debiter le compte courant"<<endl
        <<"5 - Acceder à vos comptes epargne"<<endl;
        cin>>Choix;



        if(Choix == 1)
        {
            Un.AfficherClient();
        }
        else if(Choix == 2)
        {
            Un.CompteCourant.AfficherSolde();
        }
        else if(Choix == 3)
        {
            cout<<"Montant : ";
            cin>>Credit;
            Un.CompteCourant.CrediterSolde(Credit);
        }
        else if(Choix == 4)
        {
            cout<<"Montant : ";
            cin>>Credit;
            Un.CompteCourant.DebiterSolde(Credit);
        }
        else if(Choix == 5)
        {
            if(Un.Epargne.size() == 0)
            {
                cout<<"Vous n\'avez aucun compte epargne !"<<endl;
                cout<<"Creer un compte ?"<<endl<<"1- Oui, 2- Non"<<endl;
                cin>>Choix;
                if(Choix == 1)
                    {
                        Un.Epargne.push_back(CompteEpargne());
                    }
            }else
            {
                cout<<"Vous avez un total de "<<(Un.Epargne.size())<<" Comptes"<<endl;
                cout<<"1- Affichez tout les comptes disponibles"<<endl<<"2- Afficher le solde d'un compte"<<endl
                <<"3- Cree un nouveau compte"<<endl;
                cin>>Choix;
                if(Choix == 1)
                {
                    Un.AfficherComptesEpargne();
                }
                else if(Choix == 2)
                {
                    string type;
                    cout<<"Quel compte voulez vous consulter : ";
                    cin>>type;
                    Un.AfficherSoldeCE(type);
                }
                else if(Choix == 3)
                {
                    Un.Epargne.push_back(CompteEpargne());
                }
            }
        }
    }
    return 0;
}
