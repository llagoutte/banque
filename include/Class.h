#ifndef CLASS_H
#define CLASS_H

#include <string>
#include <iostream>
#include <list>
#include <cstring>

using namespace std;

class Compte
{
    private:
        float Solde;
    public:
        Compte();
        void CrediterSolde(float);
        void DebiterSolde(float);
        void AfficherSolde();
};

class CompteCourant : public Compte
{
};

class CompteEpargne : public Compte
{
    private:
        string Type;
        float Taux, Plafond;
        bool Bloquer;
    public:
        CompteEpargne();
        void AfficherCompte();
        void AfficherTitre();
        string & ReturnType()
        {
            return Type;
        }
        float ReturnPlafond(){return Plafond;}
};

class Client
{
    public:
        Client(string, string);
        void AfficherClient();
        class CompteCourant CompteCourant;
        list<class CompteEpargne> Epargne;
        void AfficherComptesEpargne();
        void AfficherSoldeCE(string);

    private:
    string Nom, Prenom;
    int ID;
};



#endif // CLASS_H
